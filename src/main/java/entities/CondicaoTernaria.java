package entities;

import java.util.Locale;
import java.util.Scanner;

public class CondicaoTernaria {

    /*
    sintaxe: (condicao) ? valor_se_verdadeiro : valor_se_falso;
     */

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe o preço a pagar: ");
        double preco = sc.nextDouble();

        double desconto = (preco < 20 ) ? preco * 0.1 : preco * 0.05;

        System.out.printf("Valor informado = %.2f e o desconto é de %.3f", preco, desconto);
    }
}
