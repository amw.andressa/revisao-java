package entities;

import java.util.Locale;
import java.util.Scanner;

public class Operadora {

    /*
    Uma operadora de telefonia cobra 50.00 por um plano básico que dá direito a 100 minutos de telefone.
    Cada minuto que exceder a franquia de 100 minutos custa 2.00. Fazer um programa para ler a quantidade
    de minutos que uma pessoa consumiu, daí mostrar o valor a ser pago.
     */

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner scanner = new Scanner(System.in);

        System.out.print("Informe quantos minutos da operadora voce consumiu: ");
        int quantidadeMinutos = scanner.nextInt();

        double conta = 50.0;
        if (quantidadeMinutos > 100) {
            conta += (quantidadeMinutos - 100) * 2.0;
        }

        System.out.printf("Valor da conta = R$%.2f", conta);

        scanner.close();
    }

}
